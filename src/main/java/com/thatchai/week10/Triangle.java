package com.thatchai.week10;

public class Triangle extends Shape{
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c){
        super("Triangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double calArea() {
        double all = (this.a + this.b + this.c);
        double calArea = Math.sqrt(all*(all - this.a) * (all-this.b)*(all-this.c));
        return calArea;
    }

    @Override
    public double calPerimeter() {
        return this.a + this.b + this.c;
    }
    
    @Override
    public String toString(){
        return this.getName() + " a :" + this.a +" b :" + this.b + " c :" + this.c;
    }
}
